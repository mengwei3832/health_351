package com.itheima.controller;

import com.itheima.constant.MessageConst;
import com.itheima.constant.RedisConst;
import com.itheima.entity.Result;
import com.itheima.utils.SMSUtils;
import com.itheima.utils.ValidateCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

/**
 * @author mengwei
 * @date 2019/8/18
 * @desc
 */
@RestController
@RequestMapping("/validateCode")
public class ValidateCodeController {

    @Autowired
    JedisPool jedisPool;

    @RequestMapping("/send4Order")
    public Result validateCode(String telephone) {
        try {
            //获取随机验证码
            Integer validateCode = ValidateCodeUtils.generateValidateCode(4);
            //发送短信验证码
//        SMSUtils.sendSMS(SMSUtils.VALIDATE_CODE,"13261838925", String.valueOf(validateCode));
            //将验证码存入到redis中(5分钟)
            jedisPool.getResource().setex(RedisConst.SENDTYPE_ORDER + "-" + telephone,
                    5 * 60, String.valueOf(validateCode));
            return new Result(true, MessageConst.SEND_VALIDATECODE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConst.SEND_VALIDATECODE_FAIL);
        }
    }
}
