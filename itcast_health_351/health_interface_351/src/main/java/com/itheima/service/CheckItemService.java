package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckItem;

import java.util.List;

/**
 * @author mengwei
 * @date 2019/8/12
 * @desc
 */
public interface CheckItemService {

    void add(CheckItem checkItem);

    PageResult findPage(QueryPageBean queryPageBean);

    void update(CheckItem checkItem);

    void delById(Integer id);

    List<CheckItem> findAll();
}
