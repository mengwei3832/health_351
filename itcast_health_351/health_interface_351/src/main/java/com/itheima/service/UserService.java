package com.itheima.service;

/**
 * @author mengwei
 * @date 2019/8/9
 * @desc
 */
public interface UserService {

    boolean login(String username,String password);
}
