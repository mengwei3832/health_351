package com.itheima.service;

import com.itheima.pojo.OrderSetting;

import java.util.List;

/**
 * @author mengwei
 * @date 2019/8/15
 * @desc
 */
public interface OrderSettingService {

    void editOrderSettingList(List<OrderSetting> orderSettingList);

    List<OrderSetting> getOrderSettingByMonth(String currentDate);
}
