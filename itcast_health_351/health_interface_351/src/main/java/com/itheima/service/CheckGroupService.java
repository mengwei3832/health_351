package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckGroup;

import java.util.List;

/**
 * @author mengwei
 * @date 2019/8/12
 * @desc
 */
public interface CheckGroupService {


    PageResult findPage(QueryPageBean queryPageBean);

    void add(CheckGroup checkGroup, Integer[] checkitemIds);

    Integer[] findCheckItemIdsByCheckGroupId(Integer checkItemId);

    void edit(CheckGroup checkGroup, Integer[] checkitemIds);

    void delById(Integer id);

    List<CheckGroup> findAll();
}
