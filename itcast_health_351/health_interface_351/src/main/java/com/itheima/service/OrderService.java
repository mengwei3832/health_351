package com.itheima.service;

import com.itheima.entity.Result;

import java.text.ParseException;
import java.util.Map; /**
 * @author mengwei
 * @date 2019/8/18
 * @desc
 */
public interface OrderService {
    Result addOrder(Map<String, Object> map) throws Exception;

    Map<String,Object> findById(Integer id);
}
