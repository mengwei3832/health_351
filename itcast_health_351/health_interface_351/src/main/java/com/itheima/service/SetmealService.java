package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Setmeal;

import java.util.List;

/**
 * @author mengwei
 * @date 2019/8/13
 * @desc
 */
public interface SetmealService {

    /**
     * 查询全部分页数据
     */
    PageResult findPage(QueryPageBean queryPageBean);

    /**
     * 添加套餐
     */
    void add(Setmeal setmeal, Integer[] checkgroupIds);

    List<Setmeal> findAll();

    Setmeal findById(Integer id);
}
