package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.itheima.constant.MessageConst;
import com.itheima.constant.RedisConst;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.Setmeal;
import com.itheima.service.SetmealService;
import com.itheima.utils.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import redis.clients.jedis.JedisPool;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

/**
 * @author mengwei
 * @date 2019/8/13
 * @desc 套餐组
 */
@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @Reference
    SetmealService setmealService;

    @Autowired
    JedisPool jedisPool;

    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        return setmealService.findPage(queryPageBean);
    }

    @RequestMapping("/upload")
    public Result upload(MultipartFile imgFile){
        try {
            //获取原图片文件名
            String originalFilename = imgFile.getOriginalFilename();
            //获取文件后缀名
            String extendName = originalFilename.substring(originalFilename.lastIndexOf("."));
            //获取唯一名称
            String uuid = UUID.randomUUID().toString().replace("-", "");
            //拼接图片名称
            String uploadFileName = uuid + extendName;
            //上传文件
            QiniuUtils.upload2Qiniu(imgFile.getBytes(),uploadFileName);
            //图片上传已完成，把图片名称存储到redis中
            jedisPool.getResource().sadd(RedisConst.SETMEAL_PIC_RESOURCES,uploadFileName);
            return new Result(true, MessageConst.PIC_UPLOAD_SUCCESS,uploadFileName);
        } catch (IOException e) {
            e.printStackTrace();
            return new Result(false, MessageConst.PIC_UPLOAD_FAIL);
        }
    }

    @RequestMapping("/add")
    public Result add(@RequestBody Map<String,Object> map){
        JSONObject jsonObject = (JSONObject) map.get("setmeal");
        Setmeal setmeal = jsonObject.toJavaObject(Setmeal.class);
        JSONArray jsonArray = (JSONArray) map.get("checkgroupIds");
        Integer[] checkgroupIds = jsonArray.toArray(new Integer[]{});
        try {
            setmealService.add(setmeal,checkgroupIds);
            return new Result(true,MessageConst.ADD_SETMEAL_SUCCESS);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new Result(false,e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConst.ADD_SETMEAL_FAIL);
        }
    }
}
