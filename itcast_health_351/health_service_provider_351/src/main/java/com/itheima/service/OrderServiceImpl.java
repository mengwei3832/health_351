package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.constant.MessageConst;
import com.itheima.dao.MemberDao;
import com.itheima.dao.OrderDao;
import com.itheima.dao.OrderSettingDao;
import com.itheima.entity.Result;
import com.itheima.pojo.Member;
import com.itheima.pojo.Order;
import com.itheima.pojo.OrderSetting;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @author mengwei
 * @date 2019/8/18
 * @desc
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderSettingDao orderSettingDao;

    @Autowired
    OrderDao orderDao;

    @Autowired
    MemberDao memberDao;

    @Override
    public Result addOrder(Map<String, Object> map) throws Exception {
        //套餐Id
        Integer setmealId = Integer.valueOf(String.valueOf(map.get("setmealId")));
        //预约日期
        String orderDateStr = String.valueOf(map.get("orderDate"));
        Date orderDate = new SimpleDateFormat("yyyy-MM-dd").parse(orderDateStr);
        //预约类型
        String orderType = String.valueOf(map.get("orderType"));
        //手机号
        String telephone = String.valueOf(map.get("telephone"));
        //用户名
        String name = String.valueOf(map.get("name"));
        //性别
        String sex = String.valueOf(map.get("sex"));
        //身份证号
        String idCard = String.valueOf(map.get("idCard"));

        //判断预约日期是否设置
        OrderSetting orderSetting = orderSettingDao.findByDate(orderDate);
        if (orderSetting == null){
            return new Result(false, MessageConst.GET_ORDERSETTING_FAIL);
        }
        //判断预约日期是否预约已满
        if (orderSetting.getNumber() == orderSetting.getReservations()){
            return new Result(false,MessageConst.ORDER_FULL);
        }
        //判断用户是否是会员
        Member member = memberDao.findByTelephone(telephone);
        if (member == null){
            //不是会员，就创建会员（主键回显）
            member = new Member();
            member.setName(name);
            member.setSex(sex);
            member.setIdCard(idCard);
            member.setPhoneNumber(telephone);
            member.setRegTime(new Date());

            memberDao.addMember(member);
        } else {
            //是会员，判断是否重复预约
            Order order = new Order();
            order.setMemberId(member.getId());
            order.setOrderDate(orderDate);
            order.setSetmealId(setmealId);
            //查询是否有预约对象
            long count = orderDao.findByOrder(order);
            if (count > 0){
                return new Result(false,MessageConst.HAS_ORDERED);
            }
        }
        //添加预约对象
        Order order = new Order();
        order.setMemberId(member.getId());
        order.setOrderDate(orderDate);
        order.setSetmealId(setmealId);
        order.setOrderType(orderType);
        order.setOrderStatus(Order.ORDERSTATUS_NO);
        orderDao.addOrder(order);
        //预约完后，更新已预约人数
        orderSetting.setReservations(orderSetting.getReservations() + 1);
        orderSettingDao.edit(orderSetting);
        return new Result(true,MessageConst.ORDER_SUCCESS,order);
    }

    @Override
    public Map<String, Object> findById(Integer id) {
        return orderDao.findById(id);
    }
}
