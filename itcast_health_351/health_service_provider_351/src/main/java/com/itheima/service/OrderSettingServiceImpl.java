package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.OrderSettingDao;
import com.itheima.pojo.OrderSetting;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author mengwei
 * @date 2019/8/15
 * @desc
 */
@Service
public class OrderSettingServiceImpl implements OrderSettingService {

    @Autowired
    OrderSettingDao orderSettingDao;

    @Override
    public void editOrderSettingList(List<OrderSetting> orderSettingList){
        //遍历集合
        for (OrderSetting orderSetting : orderSettingList) {
            //通过日期查找数据库中预约设置对象
            OrderSetting orderSettingDB = orderSettingDao.findByDate(orderSetting.getOrderDate());
            //判断对象是否为空
            if (orderSettingDB == null){
                //为空则直接添加数据到数据库中
                orderSettingDao.add(orderSetting);
            } else {
                //不为空则比较传入的可预约人数和已预约人数
                //可预约人数
                int number = orderSetting.getNumber();
                //已预约人数
                int reservations = orderSettingDB.getReservations();
                if (reservations > number){
                    //已预约人数大于可预约人数，抛出异常
                    throw new RuntimeException("已预约人数大于可预约人数，请检查！");
                } else {
                    //已预约人数小于可预约人数，修改数据库中可预约人数
                    orderSettingDao.edit(orderSetting);
                }
            }
        }
    }

    @Override
    public List<OrderSetting> getOrderSettingByMonth(String currentDate) {
        String startDate = currentDate+"-01";
        String endDate = currentDate+"-31";
        return orderSettingDao.getOrderSettingByMonth(startDate,endDate);
    }
}
