package com.itheima.dao;

import com.itheima.pojo.OrderSetting;

import java.util.Date;
import java.util.List;

/**
 * @author mengwei
 * @date 2019/8/15
 * @desc
 */
public interface OrderSettingDao {

    /**
     * 通过日期查找数据库中预约设置对象
     */
    OrderSetting findByDate(Date orderDate);

    void add(OrderSetting orderSetting);

    void edit(OrderSetting orderSetting);

    List<OrderSetting> getOrderSettingByMonth(String startDate, String endDate);
}
