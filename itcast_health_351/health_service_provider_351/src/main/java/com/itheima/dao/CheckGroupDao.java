package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckGroup;

import java.util.List;

/**
 * @author mengwei
 * @date 2019/8/12
 * @desc
 */
public interface CheckGroupDao {

    Page<CheckGroup> findByCondition(String queryString);

    void add(CheckGroup checkGroup);

    void set(Integer id, Integer checkitemId);

    Integer[] findCheckItemIdsByCheckGroupId(Integer checkItemId);

    void edit(CheckGroup checkGroup);

    void deleteAssociation(Integer id);

    long findSetmealCountByCheckGroupId(Integer id);

    void delById(Integer id);

    List<CheckGroup> findAll();

    List<CheckGroup> findCheckGroupListBySetmealId(Integer setmealId);
}
