package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckItem;

import java.util.List;

/**
 * @author mengwei
 * @date 2019/8/12
 * @desc
 */
public interface CheckItemDao {

    void add(CheckItem checkItem);

    Page<CheckItem> findByCondition(String queryString);

    void update(CheckItem checkItem);

    void delById(Integer id);

    long findCheckItemCountById(Integer id);

    List<CheckItem> findAll();

    List<CheckItem> findCheckItemListByCheckGroupId(Integer checkGroupId);
}
