package com.itheima.dao;

import com.itheima.pojo.Member;

/**
 * @author mengwei
 * @date 2019/8/18
 * @desc
 */
public interface MemberDao {
    Member findByTelephone(String telephone);

    void addMember(Member member);
}
