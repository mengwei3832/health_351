package com.itheima.dao;

import com.itheima.pojo.Order;

import java.util.Map;

/**
 * @author mengwei
 * @date 2019/8/18
 * @desc
 */
public interface OrderDao {
    Long findByOrder(Order order);

    void addOrder(Order order);

    Map<String,Object> findById(Integer id);
}
