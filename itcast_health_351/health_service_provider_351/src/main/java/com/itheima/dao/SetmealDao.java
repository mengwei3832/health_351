package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Setmeal;

import java.util.List;

/**
 * @author mengwei
 * @date 2019/8/13
 * @desc
 */
public interface SetmealDao {
    /**
     * 查询分页所有数据
     */
    Page<Setmeal> findByCondition(String queryString);

    /**
     * 添加套餐数据
     */
    void add(Setmeal setmeal);

    /**
     * 添加套餐和检查组中间表数据
     */
    void set(Integer id, Integer checkgroupId);

    List<Setmeal> findAll();

    Setmeal findById(Integer id);
}
