package com.itheima.jobs;

import com.itheima.constant.RedisConst;
import com.itheima.utils.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPool;

import java.util.Set;

/**
 * @author mengwei
 * @date 2019/8/13
 * @desc
 */
@Component
public class ClearImgJob {

    @Autowired
    JedisPool jedisPool;

    public void clear(){
        //取两个集合的差值
        //垃圾图片的名称集合
        Set<String> sdiff = jedisPool.getResource().sdiff(RedisConst.SETMEAL_PIC_RESOURCES,
                RedisConst.SETMEAL_PIC_DB_RESOURCES);
        //删除七牛云上的垃圾图片
        for (String imgName : sdiff) {
            QiniuUtils.deleteFileFromQiniu(imgName);
            System.out.println("删除成功！");
            //清除redis中存储的垃圾图片名称
            jedisPool.getResource().srem(RedisConst.SETMEAL_PIC_RESOURCES,imgName);
        }
    }
}
